# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers
  attr_accessor :step_count
  def play
    until won?
      render
      get_move
    end
    puts "YOU HAVE WON!!! and it only took #{step_count - 1} steps!"
    render
  end

  def render
    top_row = towers.map { |tower| tower[2].nil? ? '   ' : p_render(tower[2]) }
    mid_row = towers.map { |tower| tower[1].nil? ? '   ' : p_render(tower[1]) }
    bot_row = towers.map { |tower| tower[0].nil? ? '   ' : p_render(tower[0]) }
    puts top_row.join('   ').to_s
    puts mid_row.join('   ').to_s
    puts bot_row.join('   ').to_s
    puts '[0]   [1]   [2]'
  end

  def p_render(num)
    return '=  ' if num == 1
    return '== ' if num == 2
    return '===' if num == 3
  end

  def get_move
    valid = false
    until valid == true
      puts "Please enter which tower to take from (step\##{step_count})"
      from = gets.to_i
      puts "Please enter which tower to put onto (step\##{step_count})"
      to = gets.to_i
      if valid_move?(from, to) == false
        puts 'ERROR: invalid move!!!!'
        render
      end
      valid = valid_move?(from, to)
    end
    move(from, to)
    self.step_count += 1
  end

  def initialize
    @towers = [[3, 2, 1], [], []]
    @step_count = 1
  end

  def move(from_stack, to_stack)
    @towers[to_stack].push(@towers[from_stack].pop)
  end

  def valid_move?(from_stack, to_stack)
    return false unless (0..2).cover?(from_stack) && (0..2).cover?(to_stack)
    return true if !@towers[from_stack].empty? && (@towers[to_stack].empty? ||
                   @towers[from_stack].last < @towers[to_stack].last)
    false
  end

  def won?
    @towers[0].empty? && (@towers[1].empty? || @towers[2].empty?)
  end
end

if __FILE__ == $PROGRAM_NAME
  t = TowersOfHanoi.new
  t.play
end
